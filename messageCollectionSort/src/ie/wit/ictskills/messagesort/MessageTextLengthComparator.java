package ie.wit.ictskills.messagesort;
import java.util.Comparator;

/**
 * @file MessageTextLengthComparator.java
 * @brief Comparator class implementing Comparator interface. Compares message text lengths.
 * @version 29-4-2015
 * @author jfitzgerald
 */
public class MessageTextLengthComparator implements Comparator<Message>
{

  @Override
  public int compare(Message o1, Message o2)
  {
    return Integer.compare(o1.messageText.length(), o2.messageText.length());
  }

}
