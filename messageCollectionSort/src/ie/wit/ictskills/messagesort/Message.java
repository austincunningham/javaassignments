package ie.wit.ictskills.messagesort;
/**
 * @file Message.java
 * @brief Simplified Message class implementing Comparable interface
 * @version 29-4-2015
 * @author jfitzgerald
 */
public class Message implements Comparable<Message>
{

    String messageText;

    public Message(String messageText)
    {;
        this.messageText    = messageText;
    } 

    @Override
    public String toString()
    {
        return messageText;
    }

    @Override
    public int compareTo(Message o)
    {
      return messageText.compareToIgnoreCase(o.messageText);
    }
}