package ie.wit.ictskills.messagesort;

/**
 * @file Main.java
 * @brief Creates list sample messages, sorts based on natural ordering, sorts based on message text length.
 * @version 29-4-2015
 * @author jfitzgerald
 */
import java.util.ArrayList;
import java.util.Collections;

public class Main
{

  /**
   * Prints list of Message objects
   * @param a The list of message objects
   */
  static void print(ArrayList<Message> a)
  {
    for (Message m : a)
      System.out.println(m + " ");
    System.out.println();
  }

  /**
   * Populates list with contents of primitive array of Message objects
   * @param a The array of Message objects
   * @param list The list of Message objects
   */
  static void toArray(Message[] a, ArrayList<Message> list)
  {
    for (Message m : a)
      list.add(m);
  }

  /**
   * Main method
   * @param args Standard commandline arguments array, not used here
   */
  public static void main(String[] args)
  {
    Message[] arMsg = { 
          new Message("a simple tale"), 
          new Message("z-cars go fast"), 
          new Message("gamma rays sting"),
          new Message("yeuch"), 
          new Message("Z-cars go even faster"), 
          new Message("Homo sapiens"),
          new Message("homo sapiens") 
        };

    ArrayList<Message> listMsg = new ArrayList<Message>();
    toArray(arMsg, listMsg);

    // sort using natural ordering
    System.out.println("Sorted based on natural ordering");
    System.out.println("---------------------------------------------");
    Collections.sort(listMsg);
    print(listMsg);

    // sort using message length ordering using customized comparator
    System.out.println("Sorted based on increasing messageText length");
    System.out.println("---------------------------------------------");
    Collections.sort(listMsg, new MessageTextLengthComparator());
    print(listMsg);
  }
}
