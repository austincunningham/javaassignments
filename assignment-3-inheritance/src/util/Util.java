package util;
/**
 * @file Util.java
 * @brief A compares two perimeters sizes and returns the largest.
 * 
 * @author Austin Cunningham
 * @version 2016.04.18
 */
import ie.wit.ictskills.shapes.Measurable;

import java.util.ArrayList;

public class Util
{
  /**
   * Measureable interface contains the method double perimeter(). The method
   * maximum here evalutates the single value representing the largest perimeter
   * discovered in the list of Measurable objects.
   *
   * @param object
   *          The list of objects whose classes implement the interface
   *          Measurable
   * @return Returns the largest perimeter discovered among entire list objects.
   */
  static public double maximum(ArrayList<Measurable> object)
  {
    double max = 0;
    // TODO Task 6: Implement method Util.maximum...DONE
    max = object.get(0).perimeter();
    for (Measurable obj : object)
    {
      double val = obj.perimeter();
      max = val > max ? val : max;
    }
    return max;
  }

}
