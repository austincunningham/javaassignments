package ie.wit.ictskills.shapes;
/**
 * @file Measurable.java
 * @brief A interface class Measurable to access the perimeter of a number of different shapes. 
 * @author Austin Cunningham
 * @version 2016.04.18
 */
public interface Measurable 
{
	double perimeter();
}