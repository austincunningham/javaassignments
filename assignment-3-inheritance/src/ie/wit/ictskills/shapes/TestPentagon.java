package ie.wit.ictskills.shapes;
/**
 * @file TestPentagon.java
 * @brief A test for Pentagon class that generates 4 pentagons and outputs to screen.
 * @author Austin Cunningham
 * @version 2016.04.18
 */

import java.util.ArrayList;

public class TestPentagon
{

  public static void main(String[] args)
  {
    // TODO Task 3: Display 4 cascaded Pentagons differently colored shapes...DONE

    ArrayList<Shapes> shapes = new ArrayList<>();

    shapes.add(new Pentagon(30, 60, 130, "red"));
    shapes.add(new Pentagon(40, 80, 140, "blue"));
    shapes.add(new Pentagon(50, 110, 150, "green"));
    shapes.add(new Pentagon(60, 150, 160, "black"));

    for (Shapes shape : shapes)
    {
      shape.makeVisible();
    }
  }

}
