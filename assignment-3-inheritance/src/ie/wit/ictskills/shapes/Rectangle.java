package ie.wit.ictskills.shapes;
/**
 * @file Rectangle.java
 * @brief A rectangle that can be manipulated and that draws itself on a canvas.
 * @author Austin Cunningham
 * @version 2016.04.18
 */

// TODO Task 1: Re-factor: derive from Shapes super class ... DONE

public class Rectangle extends Shapes implements Measurable
{
  private int xSideLength;
  private int ySideLength;

  public Rectangle()
  {
    super(60, 50, "red", false);
    xSideLength = 60;
    ySideLength = 30;
    setState(60, 30);
  }

  public Rectangle(int xSideLength, int ySideLength, int xPosition, int yPosition, String color)
  {
    super(xPosition, yPosition, color, false);
    setState(xSideLength, ySideLength);
  }

  public void setState(int xSideLength, int ySideLength)
  {
    this.xSideLength = xSideLength;
    this.ySideLength = ySideLength;
  }

  @Override
  public void changeSize(int scale)
  {
    if (scale > 0)
    {
      erase();
      xSideLength *= scale;
      ySideLength *= scale;
      draw();
    }
    else
    {
      System.out.println("Enter positive dimensions");
    }

  }

  @Override
  void draw()
  {
    if (isVisible)
    {
      Canvas canvas = Canvas.getCanvas();
      canvas.draw(this, color, new java.awt.Rectangle(xPosition, yPosition, xSideLength, ySideLength));
      canvas.wait(10);
    }
  }

  @Override
  public double perimeter()
  {
    
    return 2 * (xSideLength + ySideLength);
    
  }

  public static void main(String[] args)
  {

    Rectangle rectangle = new Rectangle(100, 50, 50, 100, "red");
    rectangle.draw();

  }

}