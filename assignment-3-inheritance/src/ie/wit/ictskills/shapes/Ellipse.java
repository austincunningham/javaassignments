package ie.wit.ictskills.shapes;

/**
 * @file Ellipse.java
 * @brief An ellipse that can be manipulated and that draws itself on a canvas. * 
 * @author Austin Cunningham
 * @version 2016.04.18
 */
// TODO Task 4: Complete Ellipse, inherit Shapes, implement Measurable, subclass Circle...DONE????
import ie.wit.ictskills.util.ellipse.EllipseMeasure;

import java.awt.geom.*;


public class Ellipse extends Shapes implements Measurable

{
  protected int xdiameter;
  protected int ydiameter;

  public Ellipse()
  {
    super(100, 100, "red", false);
    this.xdiameter = 100;
    this.ydiameter = 100;

  }

  public Ellipse(int xdiameter, int ydiameter, int xPosition, int yPosition, String color)
  {
    super(xPosition, yPosition, color, false);
    this.xdiameter = xdiameter;
    this.ydiameter = ydiameter;
  }

  @Override
  void draw()
  {
    Canvas canvas = Canvas.getCanvas();
    canvas.draw(this, color, new Ellipse2D.Double( xPosition, yPosition, xdiameter, ydiameter));
    canvas.wait(10);
  }

  @Override
  void changeSize(int scale)
  {
    xdiameter *= scale;
    ydiameter *= scale;
  }

  @Override
  public double perimeter()
  {
    return EllipseMeasure.perimeter(xdiameter, ydiameter);
  }

}
