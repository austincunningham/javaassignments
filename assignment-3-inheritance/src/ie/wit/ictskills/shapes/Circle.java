package ie.wit.ictskills.shapes;

import java.awt.geom.Ellipse2D;

/**
 * @file Circle.java
 * @brief A circle that can be manipulated and that draws itself on a canvas. 
 * @author Austin Cunningham
 * @version 2016.04.18
 */


public class Circle extends Ellipse implements Measurable
{
  private int diameter;

  /**
   * Create a new circle at default position with default color.
   */
  public Circle()
  {
    diameter = 90;
    xPosition = 90;
    yPosition = 90;
    color = "blue";
    isVisible = false;

  }

  public Circle(int diameter, int xPosition, int yPosition, String color)
  {

    super(diameter, diameter, xPosition, yPosition, color);

  }

  /**
   * Change the size to the new size (in pixels). Size must be >= 0.
   */
  @Override
  public void changeSize(int scale)
  {
    erase();
    diameter *= scale;
    draw();
  }

  public static void main(String[] args)
  {
    Circle circle = new Circle();
    circle.makeVisible();
    circle.draw();
  }
}
