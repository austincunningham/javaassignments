package ie.wit.ictskills.shapes;
/**
 * @file Triangle.java
 * @brief A triangle that can be manipulated and that draws itself on a canvas.
 * @author Austin Cunningham
 * @version 2016.04.18
 */
import java.awt.Polygon;

public class Triangle extends Shapes implements Measurable
{
  private int height;
  private int width;

  public Triangle()
  {
    super(150, 65, "black", false);
    this.height = 50;
    this.width = 75;
  }

  public Triangle(int height, int width, int xPosition, int yPosition, String color)
  {
    super(xPosition, yPosition, color, false);
    this.height = height;
    this.width = width;

  }

  @Override
  public double perimeter()
  {
    return 2 * Math.hypot(height, width / 2) + width;
  }

  @Override
  void draw()
  {
    Canvas canvas = Canvas.getCanvas();
    int[] xpoints = { xPosition, xPosition + (width / 2), xPosition - (width / 2) };
    int[] ypoints = { yPosition, yPosition + height, yPosition + height };
    canvas.draw(this, color, new Polygon(xpoints, ypoints, 3));
    canvas.wait(10);
  }

  @Override
  void changeSize(int scale)
  {
    if (scale > 0)
    {
      erase();
      height *= scale;
      width *= scale;
      draw();
    }
    else
    {
      System.out.println("Enter positive dimensions");
    }
  }

}
