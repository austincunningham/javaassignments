package utils;
/**
 * @file UserSocialComparator.java
 * @brief A comparator class to compare users friendships size i.e. the amount of friends in friendships array
 * @author Austin Cunningham
 * @version 2016.04.18
 */
import java.util.Comparator;

import models.User;

public class UserSocialComparator implements Comparator<User>
{
  @Override
  public int compare(User a, User b)
  {
    return Integer.compare (b.friendships.size(), a.friendships.size());
  }
}
