package utils;

/**
 * @file MessageDateComparator.java
 * @brief A comparator class to compare dates of messages(postedAt)
 * @author Austin
 * @version 2016.04.18
 */
import java.util.Comparator;

import models.Message;

public class MessageDateComparator implements Comparator<Message>
{
  @Override
  public int compare(Message a, Message b)
  {
    // TODO: Complete implementation of method
    // MessageDateComparator.compare...DONE
    return a.postedAt.compareTo(b.postedAt);
  }
}
