package utils;
/**
 * @file MessageToComparator.java
 * @brief A comparator class that facilitates comparison between the concatenated first
 * and second names of the recipient of 2 message objects Example: Message o1
 * has a User from that has String firstName and String lastName The comparison
 * is between firstName+lastName in both messages' recipient.
 * @author Austin Cunningham
 * @version 2016.04.18
 *
 */

import java.util.Comparator;

import models.Message;

public class MessageToComparator implements Comparator<Message>
{

  @Override
  public int compare(Message o1, Message o2)
  {
    
    String o1Name = o1.to.firstName + o1.to.lastName;
    String o2Name = o2.to.firstName + o2.to.lastName;
    return o1Name.compareTo(o2Name);
  }

}
