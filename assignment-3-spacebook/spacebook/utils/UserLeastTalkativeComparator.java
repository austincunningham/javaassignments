package utils;
/**
 * @file UserLeastTalkative.java
 * @brief A comparator class to compare users outbox size
 * @author Austin Cunningham
 * @version 2016.04.18
 */
import java.util.Comparator;

import models.User;

public class UserLeastTalkativeComparator implements Comparator<User>
{
  @Override
  public int compare(User a, User b)
  {
    // TODO: Complete implementation of method UserLeastTalkativeComparator.compare....DONE
    return Integer.compare (b.outbox.size(), a.outbox.size());

  }
}
