package models;

/**
* @file    LeaderBoard.java
* @brief   Class publishes users graded most socially and talkatively active
* @author Austin Cunningham
* @version 2016.04.18
*/

import java.util.ArrayList;
import java.util.Collections;

import utils.UserSocialComparator;
import utils.UserTalkativeComparator;
import views.LeaderBoardView;

public class LeaderBoard
{
  public static void index(ArrayList<User> users)
  {

    Collections.sort(users, new UserSocialComparator());
    LeaderBoardView.index(users);
  }

  public static void talkative(ArrayList<User> users)
  {
    // TODO: Complete implementation of method LeaderBoard.talkative....DONE
    Collections.sort(users, new UserTalkativeComparator());
    LeaderBoardView.talkative(users);
  }

  public static void leastTalkative(ArrayList<User> users)
  {
    // TODO: Complete implementation of method LeaderBoard.leastTalkative...DONE
    Collections.sort(users, new UserTalkativeComparator());
    Collections.reverse(users);//Descending
    LeaderBoardView.leastTalkative(users);

  }
}
