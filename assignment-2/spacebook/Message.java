
//Task 6
public class Message
{
    Subject   subject;
    String    messageText;
    User      from;
    User      to;

    public Message(Subject subject, User from, User to, String messageText)
    {
        // TODO initialize Subject field :Done
        this.from           = from;
        this.to             = to;
        this.messageText    = messageText;
        this.subject        = subject;
    } 

    public void displayMessage()
    {
        String nameFrom = from.firstName;
        String nameTo   = to.firstName;
        // TODO : Done
        System.out.println("Message subject:"+ subject +" "+ nameFrom + " says \""+ messageText + "\" to " + nameTo);
    }

    public void displayMessageContent()
    {
        System.out.println(messageText);
    }
}