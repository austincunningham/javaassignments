//Question 4 Task 3
import java.util.ArrayList;
public class Group
{
    String groupName;

    ArrayList<User> members;

    /**
     * Constructor Group
     * apply a name groupName 
     * Initialise the array members
     * @param groupName of type string object
     */
    public Group(String groupName)
    {
        //TODO
        this.groupName = groupName;
        members = new ArrayList<User>();
    }

    /**
     * Method addGroupMember add a user to a group array
     * @param user of type User object
     */
    public void addGroupMember(User user)
    {
        //TODO
        members.add(user);
    }

    /**
     * Send a message to the inbox of each group member
     * use for each to add a message to each User in the groups inbox array
     * @param message of type Message object
     */
    public void broadcastMessage(Message message)
    {
        //TODO
        for (User i : members)
        {
            i.inbox.add(message);
        }
    }

    /**
     * @return all the group names in a single string
     * Use the \n between each name to ensure printed on separate lines
     * initialise string, and used for each to poll through members array
     */
    private String groupList()
    {
        String names = "";
        //TODO
        for (User i : members)
        {
            names += i.firstName+"\n";
        }
        return names;
    }

    @Override
    public String toString() {
        return "GroupName=" + groupName + "\n" + groupList();
    } 
}
