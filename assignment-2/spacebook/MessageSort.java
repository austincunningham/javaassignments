import java.util.ArrayList;
//import java.util.Collections; can't change the api

public class MessageSort
{
    //Task 9
    /**
     * Method ot sort the Message array m Alphabetically
     * task 9 was looking for a method named public static void sortMessages(Message[] messages)
     * class TestMessaging call method MessageSort.selectionSort
     * two for loops first poll message array second compairs strings of each message
     * and calls swap if the current strings is greater that then next and polls 
     * 
     * @pram m of type Message array
     */
    public static void selectionSort(Message[] messages)
    {
        //TODO...done 
        for (int i = 0; i < messages.length; i++)
        {
            for (int j = i; j < messages.length; j++)
            { 
                if ((messages[j].messageText.compareToIgnoreCase(messages[i].messageText)) < 0)
                {
                    swap(messages, i, j);
                }
            }
        }
    }

    /**
     * method to swap positions of strings
     * @pram arMsg of type array
     * @pram to of type int
     * @pram from of type int
     */
    private static void swap(Message[] arMsg, int to, int from)
    {
        //TODO...:done
        String val = arMsg[to].messageText;// stores orignal first element
        arMsg[to].messageText = arMsg[from].messageText;//changes the first element with the value of the second
        arMsg[from].messageText = val;// puts the orignal element value into the second element
    }

    //Task 10
    /**
     *  Method to swap positions of strings
     *  same as selectionSort(Message[] messages) only with array list
     *  
     */
    public static void selectionSort(ArrayList<Message> m)
    {
        //TODO...done
        for (int i = 0; i < m.size(); i += 1)
        {
            for (int j = i; j < m.size(); j += 1)
            { 
                if ((m.get(j).messageText.compareToIgnoreCase(m.get(i).messageText) < 0))
                {
                    swap(m, i, j);
                }
            }
        }
    }

    private static void swap(ArrayList<Message> list, int to, int from)
    {
        //TODO...done
        //Collections.swap(list, to, from); can't change the api
        String val = list.get(to).messageText;
        list.get(to).messageText = list.get(from).messageText;
        list.get(from).messageText = val;
    }

}
