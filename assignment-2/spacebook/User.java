
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class User
{
    String firstName;
    String lastName;
    int age;
    String nationality;
    String email;
    String password;

    Status status;

    ArrayList<Friendship> friendships   = new ArrayList<>();
    ArrayList<Message> inbox            = new ArrayList<>();
    ArrayList<Message> outbox           = new ArrayList<>(); 

    HashMap<String, Group> groups = new HashMap<>();

    /**
     * Constructs an object intended for testing only
     * The firstName determined by caller
     * Remaining fields used default data
     */
    public User(String firstName)
    {
        setState(firstName, "Simpson", firstName+"@simpson.com", "secret");
    }

    public User(String firstName, String lastName, String email, String password)
    {
        setState(firstName, lastName, email, password);
    }  

    public void setState(String firstName, String lastName, String email, String password)
    {
        this.firstName  = firstName;
        this.lastName   = lastName;
        this.email      = email;
        this.password   = password;
        status = Status.ONLINE;//Status is an enum 

    }  
    // Task 2
    /**
     * Method setStatus applies the input status to the User status
     * @pram status of type Status object which is an enum
     */
    public void setStatus(Status status)
    {
        this.status = status;
    }

    public void broadcastMessage(Subject subject, String messageText)
    {
        for(Friendship i : friendships)
        {
            Message message = new Message(subject, this, i.targetUser, messageText);
            outbox.add(message);
            i.targetUser.inbox.add(message);
        }
    }

    // Task 3
    /**
     * Method to send message
     * Adds a message to current users outbox array
     * Adds a message to the 'to' User inbox array
     * @pram message of type Message object
     */
    public void sendMessage(Message message)
    {
        //TODO :done
        outbox.add(message);//current users outbox
        message.to.inbox.add(message);//to inbox 
    }

    public void sendMessage(Subject subject, User to, String messageText)
    {
        Message message = new Message(subject,this, to, messageText);
        outbox.add(message);
        to.inbox.add(message);
    }

    public void displayOutbox()
    {
        for(Message msg : outbox)
        {
            msg.displayMessage();
        }
    }

    public void displayInbox()
    {
        for(Message msg : inbox)
        {
            msg.displayMessage();
        }
    }
    // Task 1
    /**
     * Method befriend , checks to see if you are attempting to befriend your self or if you 
     * are trying to befriend an existing friend , if not it will add the new friend to the
     * friendships array object
     * @pram friend of type User object
     */
    public void befriend(User friend)
    {
        if(friend == this)
        {
            System.out.println("Opps! You seem to have made a mistake in attempting to befriend yourself");
        }
        else if(friendshipsContains(friend))
        {
            System.out.println("You attempted to befriend " + friend.firstName +" who is already a friend");
        }
        else
        {
            Friendship friendship = new Friendship(this, friend);
            friendships.add(friendship);
        }
    }

    // Task 1
    /**
     * Method to check weather the friendships array contains friend
     * @pram friend of type User object
     * @return boolean true or false weather friendships array contains friend
     */
    private boolean friendshipsContains(User friend)
    {
        for (int i = 0; i < friendships.size(); i++)
        {
            Friendship friendship = friendships.get(i);
            User targetUser = friendship.targetUser;
            if (friend == friendship.targetUser)
            {
                return true;
            }
        }
        return false;
    }

    public void unfriendAll()
    {
        friendships.clear();
    }

    public void unfriend(User friend) 
    {
        for(Friendship friendship : friendships)
        {
            if(friendship.targetUser == friend)
            {
                friendships.remove(friendship);
                return;
            }
        }
    }

    public void displayFriends() 
    {
        if(friendships.isEmpty())
        {
            System.out.println("Unfortunately you have no friends");
        }

        System.out.println("I'm " + this.firstName + " " + this.lastName + " and these are my \"friends\" hehe :-)");
        for(Friendship friendship : friendships)
        {
            System.out.println("My friend "+ friendship.targetUser.firstName + " is " + friendship.targetUser.status);
        }
    }

    // Task 2**
    /**
     * Method to display a list of friends filtered based on status
     * polls through the array friendships array checking Status to see if it is set to offline 
     * @pram status of type Status
     */
    public void displayFriends(Status status) 
    {
        System.out.println("Hi I'm "+ this.firstName + " "+ this.lastName + " and these are my \"friends\" He He :-)");
        for(Friendship friendship : friendships)
        {
           if (friendship.targetUser.status == Status.OFFLINE)
           {
                System.out.println("My friend "+ friendship.targetUser.firstName + " is " + friendship.targetUser.status);
           }
        }
    }

    public void addGroup(String groupName)
    {
        groups.put(groupName, new Group(groupName));
    }

    public void addGroupMember(String groupName, User user)
    {
        Group group = groups.get(groupName);
        group.addGroupMember(user);
    }

    public void broadcastMessage(String groupName, Message message)
    {
        groups.get(groupName).broadcastMessage(message);
    }

    // Task 7
    /**
     * Method to display and array list of messages
     * use for each to poll through messages array list and call displayMessage for each
     * @param message of type ArrayList<Message> object
     */
    public void displayMessages(ArrayList<Message> messages)
    {
        //TODO : done
        for ( Message i : messages)
        {
            i.displayMessage();
        }
    }

    // Task 8
    /**
     * Method to poll through messages array list and check for a subject delcared in the signature
     * @pram subject of type object Subject enum
     * @pram messages of type object ArrayList<Message>
     */
    public void displayMessages(Subject subject, ArrayList<Message> messages)
    {
        //TODO:done
        for ( Message i : messages)
        {
            if (i.subject == subject)
            {
                i.displayMessage();
            }
        }
    }

    /**
     * 
     * @param content   the content sought
     * @param msg       the array Message objects to be searched
     * @return          returns the first Message containing content, else null
     */
    public Message search(String content, ArrayList<Message> msg)
    {
        int index = 0;
        while(index < msg.size())
        {
            Message thisMsg = msg.get(index);
            if(content.equals(thisMsg.messageText))
            {
                return thisMsg;
            }
            index += 1;
        }
        return null;
    }

    /**
     * 
     * @param subject   seeking a Message object with Subject subject
     * @param msg       the array Message objects to be searched
     * @return          returns the first Message with subject matching param, else null
     */
    public Message search(Subject subject, ArrayList<Message> msg)
    {
        int index = 0;
        while(index < msg.size())
        {
            Message thisMsg = msg.get(index);
            if(subject == thisMsg.subject)
            {
                return thisMsg;
            }
            index += 1;
        }
        return null;        
    }
}