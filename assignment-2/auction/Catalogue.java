
import java.util.ArrayList;
/**
 * Class that contains and manages catalogue of auction lots
 * 
 * @author jf and Austin Cunningham
 * @version 30.3.2016
 *
 */
public class Catalogue
{
    ArrayList<Lot> lots;

    /**
     * Constructs a catalogue object
     * Initializes the Lot list field
     */
    public Catalogue()
    {
        lots = new ArrayList<Lot>();
    }

    /**
     * Adds a Lot object to list of lots field
     * @param lot The Lot object being added
     */
    public void addLot(Lot lot)
    {
        // TODO 1 Complete implementation :done
        lots.add(lot);//add lot to arraylis lots
    }

    /**
     * Constructs a string representation of this object
     * @return string representation of this object
     */
    public String toString()
    {
        String details = "Catalogue: list of lots";
        for (int i =0; i < lots.size(); i++)
        {
            details += "\n" + lots.get(i);
            // TODO 2 Complete implementation:done
        }
        return details;
    }

}
