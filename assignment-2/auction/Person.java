
/**
 * Class contains personal details of person
 * 
 * @author jf and Austin Cunningham
 * @version 30.3.2016
 *
 */
public class Person
{
    // Persons personal details
    private final String firstName;
    private final String lastName;
    String email;
    // A pin number associated with person
    private int pin;

    /**
     * Construct Person object
     * @param firstName
     * @param lastName
     * @param email
     * @param pin
     */
    public Person(String firstName, String lastName, String email, int pin)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.pin = pin;
        // TODO 1 Complete initialization with validation of remaining fields:done
    }

    /**
     * Accessor for pin number
     * @return The pin number
     */
    public int getPin()
    {
        return pin;
    }

    /**
     * Generates a String representation of Person object
     * @return The string representation of Person object
     */
    public String toString()
    {
        return "Full name: " + firstName + " " + lastName + " : Email : " + email;
    }
}
