
/**
 * Write a description of class MyMergeSort here.
 * Uses the Merge Sort algorithm to compair an array of int and swap them in assending order
 * 
 * @author Austin Cunningham
 * @version 30/3/2016
 */
public class MyMergeSort
{

    static Bid[] aux;
    private static void merge(Bid[] a, int lo, int mid, int hi)
    {
        for (int k = lo; k <= hi; k++)
        {
            aux[k] = a[k];
        }

        int i = lo;
        int j = mid + 1;
        for (int k = lo; k <= hi; k++)
        {
            if (i > mid)                                  a[k] = aux[j++];
            else if (j > hi)                              a[k] = aux[i++];
            else if (aux[j].amountBid < aux[i].amountBid) a[k] = aux[j++];
            else                                          a[k] = aux[i++];
        }
    }

    public static void sort(Bid[] a)
    {
        aux = new Bid[a.length];
        int N = a.length;

        for (int size = 1; size < N; size = size + size)
        {
            for (int lo = 0; lo < N - size; lo += size + size)
            {
                merge(a, lo, lo + size - 1, Math.min(lo + size + size - 1, N - 1));
            }
        }
    }

    public static void print(Bid[] a)
    {
        for (Bid i : a)
        {
            System.out.print(i + " ");
        }
        System.out.println();
    }

}

