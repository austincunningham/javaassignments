public class VigenereCipher
{
    private static final int NUMBER_CHARS = 26;
    private static int[][] vignereTable = new int[NUMBER_CHARS][NUMBER_CHARS];

    // vignereTable is normalized to range [0 25]. A is 65
    private static final byte TRANSFORM_UPPER = 65; 

    // the key comprising repeating keyword
    // key is normalized to range [0 25]
    private int[] keyArray;

    // the plain or message text
    private int[] plainTextArray;

    // the encrypted message text
    private int[] cipherTextArray;

    /**
     * Constructs a new VigenereCipher object.
     */
    public VigenereCipher()
    {
        generateVigenereTable();
    }

    /**
     * Generates the Vigenere Table.
     * for 2D arrays incrementing by 1 on each row would be an i + j relationship
     * This gives us our caeser shift
     * ASCII 'A' starts at 65 decimal so the lowest number in the array should be 65
     * the higest number should be 90 i.e. 'Z' 
     * mod 26 stop you going over the length of the alphabet
     */
    public static void generateVigenereTable()
    {
        for (int i = 0; i < NUMBER_CHARS; i++) 
        {
            for (int j = 0; j < NUMBER_CHARS; j++) 
            {
                vignereTable[i][j] = (TRANSFORM_UPPER +(i + j)%NUMBER_CHARS);
            }
            System.out.println();
        }
    }

    /**
     * Use the keyword to generate a key.
     * Key length is the same as the length of the plain text.
     * The key comprises repeating keyword. 
     * The last keyword in the key is truncated if necessary.
     *
     * @param keyLength The number of characters in the key.
     * @param keyword The plain text keyword from which a key will be generated.
     * @return The key for use in encrypting a message whose length matches the key length.
     */
    public String generateKey(String keyword, int keyLength)
    {
        String generateKey="";
        keyArray = new int[keyLength];
        for(int i = 0; i < keyLength;i++)
        {
            char charKey = keyword.charAt(i%keyword.length());//the mod of keyword.lenght will truncate last keyword if necessary               
            keyArray[i] = (charKey);
        }          
        for(int i : keyArray)
        {
            generateKey+=(char)(i);// used the array to generate the string generateKey
        }
        return generateKey;
    }

    /**
     * Encrypts plain text (message text) under the key using the Vigenere Table.
     * Inputs 
     * @param plainText The plain text (message text) to be encrypted
     * @param key The key under which the plain text is encrypted
     * @return The cipher text created by encrypting the plaintext under the generated key.
     */
    public String encrypt(String key, String plainText)
    {
        generateKey(key, plainText.length());//will generate keyArray
        String encrypt = "";
        cipherTextArray = new int[plainText.length()];
        for(int g = 0; g < plainText.length();g++ )
        {   
            int i = plainText.charAt(g) - TRANSFORM_UPPER;//gets each character in string plainText, taking 65 from the array value gives array index
            int j = keyArray[g] - TRANSFORM_UPPER;//gets each character in the key     
            cipherTextArray[g] = vignereTable[i][j] ;//the location in the vignere table generates cipher text
        }
        for(int i : cipherTextArray)
        {
            encrypt += (char)(i);
        }
        return encrypt;
    }

    /**
     * Decrypts cipher text under the key using the Vigenere cipher relationship.
     * 
     * @param key The key under which the cipher text is deccrypted
     * @param cipherText The cipher text to be decrypted.
     * @return The plain text obtained by decrypting the cipher text under the generated key.
     */

    public String decrypt(String key, String cipherText)
    {
        String decrypt = "";
        generateKey(key, cipherText.length());//will generate keyArray
        plainTextArray = new int[cipherText.length()];
        for(int g = 0; g < cipherText.length();g++ )
        {   
            int i = keyArray[g];
            int j = cipherTextArray[g];        	
            plainTextArray[g]= TRANSFORM_UPPER + (j - i + NUMBER_CHARS)%NUMBER_CHARS;//(cipher text - key )mod 26 
            //had to force to fall with in alphabet by adding 26, then getting the mod 26
        }
        for(int i : plainTextArray)
        {
            decrypt += (char)(i);
        }
        return decrypt;
    }

    /**
     * Print the vigenere table.
     * 
     */
    public void printVigenereTable()
    {
        for (int i = 0; i < NUMBER_CHARS; i++) 
        {
            for (int j = 0; j < NUMBER_CHARS; j++) 
            {
                System.out.printf("%c%s",(char)vignereTable [i][j]," "); //print array and cast to char
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * Print the key.
     * 
     * @param key The key used for encryption and decryption.
     */
    public void printKey(String key)
    {
        System.out.println(key);
    }

    /**
     * Print the plain text (message text).
     * 
     * @param plainText The plain text (message text).
     */
    public void printMessage(String plainText)
    {
        System.out.println(plainText);
    }

    /**
     * Print the cipher text.
     * 
     * @param cipherText The encrypted message.
     */
    public void printCipher(String cipherText)
    {
        System.out.println(cipherText);
    }

    /**
     * Test method by: 
     *  Generating and printing Vigenere Table,
     *  Generating the key using the keyword,
     *  Encrypting a message (plain text),
     *  Decrypting the cipher text.
     *  Printing the table, key, message, ciphertext & decrypted ciphertext.
     */
    public void testVigenere()
    {

        generateVigenereTable();

        String messageText = "MICHIGANTECHNOLOGICALUNIVERSITY";
        String key = generateKey("HOUGHTON", messageText.length());
        String cipherText = encrypt(key, messageText);
        String decrypted = decrypt(key, cipherText);

        printVigenereTable();
        printKey(key);
        printMessage(messageText);
        printCipher(cipherText);
        printMessage(decrypted);
    }
}