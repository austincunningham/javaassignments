
/**
 * 
 * @file    ClockDisplay.java
 * @brief 
 * The ClockDisplay class implements a digital clock display for a
 * European-style 24 hour clock. The clock shows hours, minutes and seconds . The 
 * range of the clock is 00:00:00 (midnight) to 23:59:59 (one second before 
 * midnight).
 * 
 * The clock display receives "ticks" (via the timeTick method) every second
 * and reacts by incrementing the display. This is done in the usual clock
 * fashion: the hour increments when the minutes roll over to zero.
 * 
 * 3 Methods exist to reset the display clock's hours, minutes and seconds.
 * 
 * @author Michael Kölling,David J. Barnes and Austin Cunningham
 * @version 1.0 7th Feb 2016 
 */
public class ClockDisplay
{
    private NumberDisplay hours;
    private NumberDisplay minutes;
    private NumberDisplay seconds;
    private String displayTime;    // simulates the actual display

    /**
     * Constructor for ClockDisplay objects. This constructor 
     * creates a new clock set at 00:00:00.
     */
    public ClockDisplay()
    {
        hours   = new NumberDisplay(24);
        minutes = new NumberDisplay(60);
        seconds = new NumberDisplay(60); 
        updateDisplay();
    }

    /**
     * Constructor for ClockDisplay objects. This constructor
     * creates a new clock set at the time specified by the 
     * parameters.
     */
    public ClockDisplay(int hour, int minute, int second)
    {
        hours = new NumberDisplay(24);
        minutes = new NumberDisplay(60);
        seconds = new NumberDisplay(60);
        setTime(hour, minute, second);
    }

    /**
     * This method should get called once every Second - it makes
     * the clock display go one second forward.
     */
    public void timeTick()
    {
        seconds.incrementValue();
        //when seconds value is zero it means time to increment minuits
        if(seconds.getValue() == 0)
        {
            minutes.incrementValue();
        }
        //when minutes and seconds are both zero means time to increment hours    
        if((minutes.getValue() == 0) && (seconds.getValue() == 0))
        {
            hours.incrementValue();
        }
        updateDisplay();
    }

    /**
     * Set the time of the display to the specified hour,
     * minute and  second.
     */
    public void setTime(int hour, int minute, int second)
    {
        hours.setValue(hour);
        minutes.setValue(minute);
        seconds.setValue(second);
        updateDisplay();
    }

    /**
     * Return the current time of this display in the format HH:MM:SS.
     */
    public String getTime()
    {
        return displayTime;
    }

    /**
     * Update the internal string that represents the display.Seconds added.
     */
    private void updateDisplay()
    {
        displayTime =hours.display() + ":" + minutes.display() + ":"+ seconds.display();
    }

    /**
     *  the next 3 methods recive 'value' from similar named methods in class clock and passes value to setValue method in the NumberDisplay Class 
     *  which will reset the respective hours minutes and seconds to the same value as value.
     */
    public void resetSeconds(int value)
    {
        seconds.setValue(value);
    }

    public void resetMinutes(int value)
    {
        minutes.setValue(value);
    }

    public void resetHours(int value)
    {
        hours.setValue(value);
    }
}
