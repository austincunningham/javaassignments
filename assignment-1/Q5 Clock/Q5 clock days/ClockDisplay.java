
/**
 * @file    ClockDisplay.java
 * @brief 
 * The ClockDisplay class implements a digital clock display for a
 * European-style 24 hour clock. The clock shows hours and minutes. The 
 * range of the clock is 000:00:00:00 (midnight) to 364:23:59:59 (one minute before 
 * midnight).
 * 
 * The clock display receives "ticks" (via the timeTick method) every minute
 * and reacts by incrementing the display. This is done in the usual clock
 * fashion: the hour increments when the minutes roll over to zero.
 * 
 * @author Michael Kölling, David J. Barnes and Austin Cunningham
 * @version 1.0 8th Feb 2016
 */
public class ClockDisplay
{
    private NumberDisplay days;
    private NumberDisplay hours;
    private NumberDisplay minutes;
    private NumberDisplay seconds;
    private String displayTime;    // simulates the actual display

    /**
     * Constructor for ClockDisplay objects. This constructor 
     * creates a new clock set at 000:00:00:00.
     */
    public ClockDisplay()
    {
        days    = new NumberDisplay(365);
        hours   = new NumberDisplay(24);
        minutes = new NumberDisplay(60);
        seconds = new NumberDisplay(60);
        updateDisplay();
    }

    /**
     * Constructor for ClockDisplay objects. This constructor
     * creates a new clock set at the time specified by the 
     * parameters.
     */
    public ClockDisplay(int hour, int minute, int second, int day)
    {
        hours = new NumberDisplay(24);
        minutes = new NumberDisplay(60);
        setTime(day, hour, minute, second);
    }

    /**
     * This method should get called once every second - it makes
     * the clock display go one second forward.
     */
    public void timeTick()
    {
        seconds.incrementValue();
        //when seconds value is zero it means time to increment minuits
        if(seconds.getValue() == 0)
        {
            minutes.incrementValue();
        }
        //when minutes and seconds are both zero means time to increment hours    
        if((minutes.getValue() == 0) && (seconds.getValue() == 0))
        {
            hours.incrementValue();
        }
        //when hours , minutes and seconds are all zero means time to increment days

        if((hours.getValue() == 0 ) && (minutes.getValue() == 0) && (seconds.getValue() == 0))
        {
            days.incrementValue();
        }
        updateDisplay();
    }

    /**
     * Set the time of the display to the specified day ,hour, minute
     * and second.
     */
    public void setTime(int hour, int minute, int second, int day)
    {
        hours.setValue(hour);
        minutes.setValue(minute);
        seconds.setValue(second);
        days.setValue(day);
        updateDisplay();
    }

    /**
     * Return the current time of this display in the format DDD;HH:MM:SS.
     */
    public String getTime()
    {
        return displayTime;
    }

    /**
     * Update the internal string that represents the display.
     */
    private void updateDisplay()
    {
        displayTime = days.display()+ ":" + hours.display() + ":" + minutes.display() + ":"+ seconds.display();
    }

    /**
     *  the next 4 methods recive 'value' from similar named methods in class clock and passes value to setValue method in the NumberDisplay Class 
     *  which will reset the respective hours minutes and seconds to the same value as value.
     */
    public void resetSeconds(int value)
    {
        seconds.setValue(value);
    }

    public void resetMinutes(int value)
    {
        minutes.setValue(value);
    }

    public void resetHours(int value)
    {
        hours.setValue(value);
    }

    public void resetDays(int value)
    {        
        days.setValue(value);    
    }
}
