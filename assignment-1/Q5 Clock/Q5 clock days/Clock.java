import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * @file    Clock.java
 * @brief  
 * A very simple GUI (graphical user interface) for the clock display.
 * In this implementation, time runs at about 3 minutes per second, so that
 * testing the display is a little quicker.
 * 
 * @author Michael Kölling, David J. Barnes and Austin Cunningham
 * @version 1.0 8th Feb 2016
 */
public class Clock
{
    private JFrame frame;
    private JLabel label;
    private ClockDisplay clock;
    private boolean clockRunning = false;
    private TimerThread timerThread;
    private ClockDisplay seconds; // three new objects for resetting 
    private ClockDisplay minutes;
    private ClockDisplay hours;
    private ClockDisplay days;

    /**
     * Constructor for objects of class Clock
     */
    public Clock()
    {
        makeFrame();
        clock = new ClockDisplay();
    }

    /**
     * 
     */
    private void start()
    {
        clockRunning = true;
        timerThread = new TimerThread();
        timerThread.start();
    }

    /**
     * 
     */
    private void stop()
    {
        clockRunning = false;
    }

    /**
     * 
     */
    private void step()
    {
        clock.timeTick();
        label.setText(clock.getTime());
    }

    /**
     *  the next 4 methods reset the seconds , minutes, hour, and days by setting value.
     *  needed to set the new objects (seconds,minutes,hours,days) equal to the ClockDisplay clock,
     *  as creating a new ClockDisplay did 000:00:00:00, which is not what we want.
     *  then use a modified step() for each. These pass value to similar named methods 
     *  in ClockDisplay
     */
    public void resetSeconds(int value)
    {
        int i =1;
        seconds = clock;
        if (value == 0)
        {
            seconds.resetSeconds(value); //increment can't work for minus numbers
        }
        else
        {
            seconds.resetSeconds(value-i);//value will not be seen unless decremented to factor for seconds increment duing getTime call
        }
        label.setText(seconds.getTime());
    }

    /**
     * reset the minutes and update the display
     */
    public void resetMinutes(int value)
    {
        minutes = clock;
        minutes.resetMinutes(value);
        label.setText(minutes.getTime());
    }

    /**
     * reset the hours and update the display
     */
    public void resetHours(int value)
    {
        hours = clock;
        hours.resetHours(value);
        label.setText(hours.getTime());
    }

    /**
     * reset the days and update the display
     */
    public void resetDays(int value)
    {
        days = clock;
        days.resetDays(value);
        label.setText(days.getTime());
    }
    // 
    /**
     * 'About' function: show the 'about' box.
     */
    private void showAbout()
    {
        JOptionPane.showMessageDialog (frame, 
            "Clock Version 1.0\n" +
            "A simple interface for the 'Objects First' clock display project",
            "About Clock", 
            JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Quit function: quit the application.
     */
    private void quit()
    {
        System.exit(0);
    }

    /**
     * Create the Swing frame and its content.
     */
    private void makeFrame()
    {
        frame = new JFrame("Clock (Days : Hours : Minutes : Seconds");
        JPanel contentPane = (JPanel)frame.getContentPane();
        contentPane.setBorder(new EmptyBorder(1, 90, 1, 90));

        makeMenuBar(frame);

        // Specify the layout manager with nice spacing
        contentPane.setLayout(new BorderLayout(12, 12));

        // Create the image pane in the center
        label = new JLabel("000:00:00:00", SwingConstants.CENTER);
        Font displayFont = label.getFont().deriveFont(96.0f);
        label.setFont(displayFont);
        //imagePanel.setBorder(new EtchedBorder());
        contentPane.add(label, BorderLayout.CENTER);

        // Create the toolbar with the buttons
        JPanel toolbar = new JPanel();
        toolbar.setLayout(new GridLayout(1, 0));

        JButton startButton = new JButton("Start");
        startButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { start(); }
            });
        toolbar.add(startButton);

        JButton stopButton = new JButton("Stop");
        stopButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { stop(); }
            });
        toolbar.add(stopButton);

        JButton stepButton = new JButton("Step");
        stepButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { step(); }
            });
        toolbar.add(stepButton);
        
         JButton maxSecButton = new JButton("Max Sec");
        maxSecButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { resetSeconds(59); }//new button to set the seconds
            });
        toolbar.add(maxSecButton);
        
        JButton maxMinButton = new JButton("Max Min");
        maxMinButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { resetMinutes(59); }//new buttons to set the minutes
            });
        toolbar.add(maxMinButton);
        
        JButton maxHrsButton = new JButton("Max Hrs");
        maxHrsButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { resetHours(23); }//new button to set the hours
            });
        toolbar.add(maxHrsButton);
        
        JButton maxDaysButton = new JButton("Max Days");
        maxDaysButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { resetDays(364); }//new button to set the hours
            });
        toolbar.add(maxDaysButton);


        // Add toolbar into panel with flow layout for spacing
        JPanel flow = new JPanel();
        flow.add(toolbar);

        contentPane.add(flow, BorderLayout.SOUTH);

        // building is done - arrange the components      
        frame.pack();

        // place the frame at the center of the screen and show
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(d.width/2 - frame.getWidth()/2, d.height/2 - frame.getHeight()/2);
        frame.setVisible(true);
    }

    /**
     * Create the main frame's menu bar.
     * 
     * @param frame   The frame that the menu bar should be added to.
     */
    private void makeMenuBar(JFrame frame)
    {
        final int SHORTCUT_MASK =
            Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

        JMenuBar menubar = new JMenuBar();
        frame.setJMenuBar(menubar);

        JMenu menu;
        JMenuItem item;

        // create the File menu
        menu = new JMenu("File");
        menubar.add(menu);

        item = new JMenuItem("About Clock...");
        item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { showAbout(); }
            });
        menu.add(item);

        menu.addSeparator();
        
         menu.addSeparator();

        item = new JMenuItem("Reset seconds to 00");
        item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { resetSeconds(0); }//new menu to reset seconds
            });
        menu.add(item);
        
        menu.addSeparator();

        item = new JMenuItem("Reset minutes to 00");
        item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { resetMinutes(0); }//new menu to reset minutes
            });
        menu.add(item);
        
        menu.addSeparator();

        item = new JMenuItem("Reset hours to 00");
        item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { resetHours(0); }//new menue to reset hours
            });
        menu.add(item);
        
        menu.addSeparator();

        item = new JMenuItem("Reset days to 000");
        item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { resetDays(0); }//new menue to reset days
            });
        menu.add(item);
        
        menu.addSeparator();

        item = new JMenuItem("Quit");
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, SHORTCUT_MASK));
        item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { quit(); }
            });
        menu.add(item);
    }

    class TimerThread extends Thread
    {
        public void run()
        {
            while (clockRunning) {
                step();
                pause();
            }
        }

        private void pause()
        {
            try {
                Thread.sleep(300);   // pause for 300 milliseconds
            }
            catch (InterruptedException exc) {
            }
        }
    }
}