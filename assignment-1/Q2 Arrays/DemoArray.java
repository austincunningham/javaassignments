import java.util.ArrayList;
import java.util.Iterator;
/**
 * @file    DemoArray.java
 * @brief   This class describes a various actions on preformed on arrays includes 
 *                  methods using for , while, do while and sum the even numbers in arrays.
 * @version 1.0 Feb 5, 2016
 * @author  Austin Cunningham
 */
public class DemoArray {
    private int[] intArray;
    private int size = 10;        

    public DemoArray() 
    {
        intArray = new int[size];
    }

    /**
     * Use a for loop to initialize each element in the array with 100 + i.
     * Use a similar loop to output the array.
     */
    public void demoArray() 
    {
        for( int i = 0; i < intArray.length ; i++)
        {
            intArray[i] = 100 +i;
        }
        for(int j = 0;j <intArray.length ; j++)
        {
            System.out.println("Element at index "+j+" is "+intArray[j]);
        }
    }

    /**
     * Use a while loop to initialize each element in the array with 200 + 2*i.
     * Use a similar loop to output the array.
     */
    public void demoArray2() 
    {
        int i =0;
        int j =0;
        while (i< intArray.length)
        {
            intArray[i] = 200 + (2*i);

            i++;
        }
        while(j < intArray.length)
        {
            System.out.println("Element at index "+j+" is "+intArray[j]);
            j++;
        }
    }

    /**
     * Use a do-while loop to initialize each element in the array with 300 + 3*i.
     * Use a similar loop to output the array.
     */
    public void demoArray3() 
    {
        int i = 0;
        int j =0;
        do
        {
            intArray[i] = 300+(3*i);
            i++;
        }while(i<intArray.length);
        do
        {
            System.out.println("Element at index "+j+" is "+intArray[j]);
            j++;
        }while(j<intArray.length);
    }

    /**
     * Use an iterator to initialize each element in the array with 400 + 4*i.
     * Use a similar loop to output the array.
     */
    public void demoArray4() 
    {   
        int i = 0;
        int j = 0;
        ArrayList<Integer> list = new ArrayList<Integer>();

        for(i=0; i<size; i++)
        {
            list.add(400 +(4*i));

        }
        Iterator iterat = list.iterator();
        while(iterat.hasNext())
        {
            System.out.println("Element at index "+j+" is "+ iterat.next());
            j++;
        }

    }

    /**
     * Create an array of even numbers in range [low, hi]
     * Precondition: low and hi must be even. 
     * Enforce precondition: print message if not met and return immediately.
     * Calculate and print the sum of these numbers.
     * 
     * @param low The minimum range value inclusive
     * @param hi The maximum range value inclusive
     */
    public void sumEven(int low, int hi)
    {
        int length = 0;
        // Check preconditions. If not complied with print message and return.
        if (low%2 ==0 && hi%2 ==0 && hi > low)
        {
            for (int i = low; i <= hi; i=(i+2))
            {
                length ++;// array length will be zero if conditions not met
            }

        }
        else// Message: Please enter even arguments
        {
            System.out.println("make sure that your numbers are even and your hi is larger that your low");
        }
        // Declare and allocate memory to array of int type
        int[] intArray2;
        intArray2 = new int[length];
        int i = 0;
        int sum =0;
        int lowest = low;//print statment needs low unchanged so will copy it
        // In a loop, initialize the array with even numbers in range [lo, hi]
        while( i < intArray2.length)
        {
            intArray2[i] = lowest;
            //System.out.println(low+" array content");
            lowest = lowest +2;
            i++;            
        }
        // In a second loop. accessing the above array, calculate the cumulative sum of the even numbers
        for (int j : intArray2)
        {
            sum += j;
            //System.out.println( j+"sum "+ sum);
        }
        // Print the sum as part of a message structured as follows, for example, where
        // range is [0, 10] and consequently sum is 30
        // Sum even numbers from 0 to 10 is 30
        System.out.println("\nSum even numbers from " +low+ " to " +hi+ " is " +sum);
    }
}


