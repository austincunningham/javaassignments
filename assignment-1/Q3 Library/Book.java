/**
 * @file    Book.java
 * @brief   This class defines the basic description of a book and provides some characteristic methods.
 * @version 1.0  6 feb 2016 
 * @author  Austin Cunningham 
 */
public class Book
{
    private String   title;
    private String   author;
    private String   isbn;
    private int      numberPages;
    private boolean  borrowed =false;
    private int      numberBorrowings;

    public Book(String title, String author, int numberPages, String isbn)
    {
        this.title = title;
        this.author = author;
        this.numberPages = numberPages;
        this.isbn = isbn;
    }

    /**
     * Method to set borrowed to true and increment numberBorrowings
     */
    public void borrow()        // borrow a book 
    {
        borrowed = true;
        numberBorrowings++;
    }

    /**
     * Method to check if a book is borrowed
     * @pram borrowed
     */
    public boolean isBorrowed() // is book on loan?
    {
        return borrowed;
    }

    /**
     *  Method to return book
     *  sets borrowed status to false
     */
    public void returns()       // return book 
    {
        borrowed = false;
    }

    /**
     * Method to check the loan status of a book, checks the borrowed status to return a result
     * string 
     */
    public String loanStatus()  // print a message
    {
        if(borrowed == true )
        {
            return " is not available: presently on loan";
        }
        else
        {
            return "available ";
        }
    }

    /**
     * Method to print the all details of a book
     */
    public void printDetails()  // print details re book
    {
        System.out.println("\nTitle        : "+title);
        System.out.println("Author       : "+author);
        System.out.println("Pages        : "+numberPages);
        System.out.println("ISB          : "+isbn);
        System.out.println("Borrowed     : "+numberBorrowings+" times");
        System.out.println("Availability : "+title+ " is "+loanStatus());
    }

    /**
     * Method to get the title 
     * @pram title
     */
    //public void getTitle()      // return book title 
    public String getTitle()
    {
        //System.out.println(title);
        return title;
    }

}
