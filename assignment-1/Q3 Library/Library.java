import java.util.ArrayList;
import java.util.Iterator;
/**
 * @file    Library.java
 * @brief   This class defines the basic description of a libary and provides some characteristic methods.
 * @version 1.0 4/feb/2016
 * @author  Austin Cunningham 
 */ 

public class Library
{
    private ArrayList<Book> books;
    private Book valueObj;

    public Library()
    {
        books = new ArrayList<Book>();
    }

    /**
     * Method to add a book to an array books 
     */
    public void add(Book book)
    {
        books.add(book);
        valueObj=books.get(books.size() - 1);//last element in arraylist updated valueObj, when you add to an arraylist it becomes the last element
    } 

    /**
     * Method to determine the size of the books array
     */
    public int numberBooks()
    {
        return books.size();
    }

    /**
     * Method to check the loan status , call from Books class
     * @pram valueObj.loanStatus();
     */
    public String loanStatus(Book book)
    {
        return valueObj.loanStatus();
    }

    /**
     * Method to check to see if Libary contains a book , returns a string checks using contains method which returns a boolean
     */
    //public boolean hasBook(Book book) 
    public String hasBook(Book book)
    {
        if (books.contains(book))
        {
            //System.out.println(books.get(books.size() - 1));
            return "Book title "+valueObj.getTitle()+" in stock and available to borrow";
        }
        else
        {
            return "We do not stock book title "+valueObj.getTitle();        
        }
    }

    /**
     * Method to remove a book from the library, 
     */
    public boolean removeBook(Book book)
    {
        return books.remove(book);//remove a book of index book
    }

    /**
     * Method to remove all books from the lirary
     */
    public void removeAllBooks()
    {
        books.clear();// clear the array list
    }

    /**
     * Method to print the details of the Library , use iterator to go through the books array andc call the printDetails from Books class.
     */
    public void printDetailsAll()// librarian calls library.printDetailsAll so was change to this, as this was orignally library.printDetails, 
    {
        Iterator<Book> iterat = books.iterator();
        while(iterat.hasNext())
        {
            valueObj = iterat.next();
            valueObj.printDetails();
        }
    }
}

