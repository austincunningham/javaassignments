import java.lang.Math;
/**
 * @file    Sphere.java
 * @brief   This class descibes a sphere and includes
 *                    methods to determine volume, surface area, circumference , number of circles created.
 * 
 * @author  Austin Cunningham 
 * @version 1.0 Febuary 2nd, 2016
 */
public class Sphere
{
    double radius = -10;
    static int numberSpheres = 0;
    int set =0;

    /**
     * Set the default Sphere radius to 10
     */
    public Sphere()
    {
        setState(10.0);
    }

    /**
     * Use setState to set the radius of the Sphere
     */
    public Sphere(double radius)
    {
        setState(radius);
    }

    /**
     * Method to set the state of the Sphere
     */
    public void setState(double radius)
    {
        this.radius = radius;
        isValid(radius);
        set++;
    }

    /**
     * Method to get the surface area
     * @return four PI radius squared
     */
    public double surfaceArea()
    {
        double four=4.0;
        return four*Math.PI*radius*radius;
    }

    /**
     * Method to get the volume of a sphere
     * @return the volume of sphere
     */ 
    public double volume()
    {
        double fourThirds = 4.0/3.0;
        double cubed = 3.0;
        return fourThirds * Math.PI * Math.pow(radius,cubed);
    }

    /**
     * Method to get the circumference of great circle
     * @return the circumferenc
     */
    public double greatCircle()
    {
        double two = 2.0;
        return two * Math.PI * radius;
    }

    /**
     * Method too change the size of an existing sphere
     * @pram radius
     */
    public void changeSize(double radius)
    {
        this.radius = radius;
    }

    /**
     * Method to check the number of spheres
     * @pram numberSpheres
     */
    public int getNumberSpheres()
    {
        if(numberSpheres == set)
        {
            return numberSpheres++;
        }
        else
        {
            return numberSpheres;
        }
    }

    /**
     * Method to check if the input is valid
     * System.exit(0) will close program if negative value is inputted 
     */
    private boolean isValid(double value)
    {
        if( radius > 0)
        {
            getNumberSpheres();
            return true;
        }
        else
        {
            System.out.println("You have attempt to create a sphere with a negative radius\nOnly positive radii permitted");
            System.exit(0); //kills the object no need to decremnet set.
            return false; // no need for this other that to get the code to compile to meet the API
        }
    }

}

