/**
 * @file    ICTStrings.java
 * @brief   This class manipulates compairs and contrasts Strings it includes  
 *                  methods to compair two strings, parts of strings, return 
 *                  only parts of strings ,reverse strings order and determine
 *                  if a string is a palindrome
 *               
 * @version 1.0 Febuary 3, 2016
 * @author  Austin Cunningham
 */

public class ICTStrings
{
    /**
     * Check is s1 is equal to s2
     * @param s1 a string
     * @param s2 another string
     * @return true if strings equal else false
     */
    public static boolean isEqual(String s1, String s2)
    {
        return s1.equals(s2);        
    }

    /**
     * Check is s1 is equal to s2 ignoring case
     * @param s1 a string
     * @param s2 another string
     * @return true if strings equal, ignoring case, else false
     */
    public static boolean isEqualIgnoreCase(String s1, String s2)
    {
        return s1.equalsIgnoreCase(s2);
    }

    /**
     * Check if s1 has prefix
     * @param a string
     * @param another string
     * @return true
     */
    public static boolean hasPrefix(String s1, String prefix)
    {
        return s1.contains(prefix);       
    }

    /**
     * Length string comprising concatenated strings s1 and s2
     * @param s1 a string
     * @param s2 another string
     * @return length of s1 concatenated with s2
     */
    public static int length(String s1, String s2)
    {
        int concatLength = s1.length()+s2.length();
        return concatLength;
    }

    /**
     * Create string the uppercase version of s1
     * @param s1 a string
     * @return copy of s1 all in upper case
     */
    public static String toUpper(String s1)
    {
        return s1.toUpperCase();
    }

    /**
     * Returns true if string ends with suffix.
     * Example if s1 is "this is a string" and suffix is "ring" the return value will be true.
     * @param s1 a string
     * @param suffix a string
     * @return true if s1 ends in suffix
     */
    public static boolean endsWith(String s1, String suffix)
    {
        return s1.contains(suffix);
    }

    /**
     * Create substring of s1 from indexStart to indexEnd excluding indexEnd
     * That is: range is [indexStart, indexEnd)
     * Example String "Hello ICTSkills"
     * indexStart = 2;
     * indexEnd = 8;
     * substring is "llo IC"
     * @param s1 a string
     * @param indexStart of s1 becomes zeroth index of substring
     * @param indexEnd of s1 determines end of substring
     * @return the substring
     */
    public static String subString(String s1, int indexStart, int indexEnd)
    {
        return s1.substring(indexStart, indexEnd); 
    }

    /**
     * Create a string, the reverse of string using StringBuilder
     * @param string the string to be reversed
     * @return a copy of string reversed
     */
    public static String reverse_1(String string)
    {
        return new StringBuilder(string).reverse().toString();
    }

    /**
     * Create a string, the reverse of string
     * An array of char type of same size of string is instantiated.
     * In a for loop the string characters are assigned to the array in reverse order.
     * The array is used to create a string which is then returned.
     * This string will be the reverse of the actual parameter string.
     * 
     * @param string the string to be reversed
     * @return a copy of string reversed
     */
    public static String reverse_2(String string)
    {
        String reverse="";
        for(int i = string.length() -1; i>=0; i--)
        {
            reverse = reverse + string.charAt(i);
        }
        return reverse;
    }

    /**
     * Create a string, the reverse of string
     * Use a StringBuffer class.
     * @param string the string to be reversed
     * @return a copy of string reversed
     */
    public static String reverse_3(String string)
    {       
        return new StringBuffer(string).reverse().toString();
    }

    /** 
     * Check if string is a palindrome
     * @param s Is s a palindrome?
     * @return Returns true if argument a palindrome, else false.
     */
    public static boolean isPalindrome(String s)
    {
        s = s.replaceAll("[^a-zA-Z]", "");

        return s.equalsIgnoreCase(new StringBuilder(s).reverse().toString());
    }

}
