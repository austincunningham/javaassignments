package ie.wit.ictskills.shapes;

// TODO Task 1: Refactor: derive from Shapes super class

public class Rectangle
{
    private int xSideLength;
    private int ySideLength;
    private int xPosition;
    private int yPosition;
    private String color;
    private boolean isVisible;

    public Rectangle()
    {
        xSideLength = 60;
        ySideLength = 30;
        xPosition = 60;
        yPosition = 50;
        color = "red";
        isVisible = false;
        setState(60, 30, 60, 50, "red"); 
    }

    public Rectangle(int xSideLength, int ySideLength, int xPosition, int yPosition, String color)
    {
        setState(xSideLength, ySideLength, xPosition, yPosition, color);
    }

    public void setState(int xSideLength, int ySideLength, int xPosition, int yPosition, String color)
    {
        this.xSideLength = xSideLength;
        this.ySideLength = ySideLength;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.color = color;
        isVisible = true;
    }

    public void makeVisible()
    {
        isVisible = true;
        draw();
    }

    public void makeInvisible()
    {
        erase();
        isVisible = false;
    }
    
    
    public void changeSize(int xSideLength, int ySideLength)
    {
        if(xSideLength > 0 && ySideLength > 0) 
        {
            erase();
            this.xSideLength = xSideLength;
            this.ySideLength = ySideLength;
            draw();
        }
        else
        {
            System.out.println("Enter positive dimensions");
        }

    }

    public void changeColor(String color)
    {
        this.color = color;
        draw();
    }

    private void draw()
    {
        if(isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.draw(this, color,
                    new java.awt.Rectangle(xPosition, yPosition, xSideLength, ySideLength));
            canvas.wait(10);
        }
    }

    private void erase()
    {
        if(isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.erase(this);
        }
    }
}